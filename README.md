# Fenx Blog Source Code
Aquí esta el código fuente de [mi blog/sitio web](https://f3nx.neocities.org/ "mi blog/sitio") basado en el trabajo de [ollieparanoid](https://github.com/ollieparanoid/ollieparanoid.github.io "ollieparanoid"), lanzada bajo la **GNU Affero General Public License v3.0**
## Como usarlo
Primero hay que instalar `make` con `sudo apt install make` o `sudo pacman -S make` (Según uses Debian u Arch)

Ahora para generar las pages/post debes ir a la carpeta raíz y ejecutar el Makefile con `make` ahora tendrás todas las carpetas generadas (/page/ /post/ index.html feed.xml) en el directorio anterior a la raiz.

## Configuraciones
La carpeta /template/ es la plantilla del sitio, sus archivos .html son super hackeables :3 (Solo es html y css, nada de js)

El Makefile; el generador del sitio, debes tener paciencia y entender algo de scripting pero, mirando y rompiendo se aprende (Necesito mejorar los comentarios)
## Dudas
Cualquier duda me la puedes mandar a mi correo que esta en mi blog :3